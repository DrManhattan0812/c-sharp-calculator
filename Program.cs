﻿using System;

// Code de ROHEE Guillaume
namespace LPRGI_Algo_Calculatrice
{
    class Program
    {
        // Filtre l'opération entrée par l'utilisateur et remplace les "." par des "," pour anticiper des problèmes de conversion.
        static string FiltrerVirgules(string operation)
        {
            string operationFiltre = "";

            for (int i = 0; i < operation.Length; i++)
            {
                if(operation[i] == '.')
                {
                    operationFiltre += ',';
                }
                else
                {
                    operationFiltre += operation[i];
                }
            }

            return operationFiltre;
        }

        // Mise à jour de la chaîne opération fournie en paramtère pour la remplacée par le résultat du calcul.
        static string MaJOperation(string operation, int[] calcFlag, string calcRes)
        {
            string tmpOperation = "";

            // Boucle jusqu'à l'extrémité gauche du calcul réalisé (calcFlag[1]) et copie de l'opération.
            for (int i = 0; i < calcFlag[0] + 1; i++)
            {
                tmpOperation += operation[i];
            }

            // insertion du résultat (calcRes)
            tmpOperation += calcRes.ToString();

            // Boucle de l'extrémité droite du calcul (calcFlag[2]) et copie du reste de l'opération.
            for (int i = calcFlag[1]; i < operation.Length; i++)
            {
                tmpOperation += operation[i];
            }

            return tmpOperation;
        }

        // Procédure permettant de s'assurer qu'il n'est pas possible de diviser par 0.
        static void CheckDivZero(string[] calcElements, char operateur)
        {
            if(calcElements[1] == "0" && operateur == '/')
            {
                Console.WriteLine("Erreur : division par 0 impossible.");
                System.Environment.Exit(1);
            }
        }

        // Fonction chargée de calculer l'opération fournie. 
        static double RealiserOperation(string[] calcElements, char operateur)
        {
            double calcRes = 0;

            // Vérification de la présence d'une division par 0.
            CheckDivZero(calcElements, operateur);

            try{
                switch (operateur) // Réalisation du calcul selon l'opérateur fourni en paramètre. 
                {
                    case '+': { calcRes = Convert.ToDouble(calcElements[0]) + Convert.ToDouble(calcElements[1]); break; }
                    case '-': { calcRes = Convert.ToDouble(calcElements[0]) - Convert.ToDouble(calcElements[1]); break; }
                    case '*': { calcRes = Convert.ToDouble(calcElements[0]) * Convert.ToDouble(calcElements[1]); break; }
                    case '/': { calcRes = Convert.ToDouble(calcElements[0]) / Convert.ToDouble(calcElements[1]); break; }
                }
            }
            catch (Exception e) // Gestion de l'exception selon laquelle l'utilisateur n'aurait pas rentré une chaîne correcte.
            {
                Console.WriteLine("Le calcul ne peut pas être résolu car ses opérandes sont incorrectes");
                System.Environment.Exit(2);
            }

            return calcRes;
        }

        // Récupère le membre gauche du calcul.
        static string GetOperandeGauche(string calcul, int operatorIndex, int[] calcFlag)
        {
            int i = operatorIndex - 1;
            string tmpOperande = ""; // Accueil les éléments du membre de gauche qui seront à l'envers
            string operande = "";

            // boucle tant qu'aucun opérateur est rencontré et que le bord gauche n'a pas été atteint.
            while (i >= 0 && (calcul[i] != '*' && calcul[i] != '/' && calcul[i] != '+' && calcul[i] != '-'))
            {
                tmpOperande = tmpOperande + calcul[i]; // Copie des éléments à gauche de l'opérateur (ils sont donc à l'envers).       
                i--;
                calcFlag[0] = i; // Mise à jour de la case de calcFlag comportant l'index où se trouve l'extrémité du membre gauche du calcul.
            }

            // Copie des éléments dans le bon ordre (inversion).
            for(int j = tmpOperande.Length - 1; j >= 0; j--)
            {
                operande = operande + tmpOperande[j];
            }

            return operande;
        }

        // Récupère le membre droit du calcul.
        static string GetOperandeDroite(string calcul, int operatorIndex, int[] calcFlag)
        {
            int i = operatorIndex + 1;
            string operande = "";

            // boucle tant qu'aucun opérateur est rencontré et que le bord droit n'a pas été atteint.
            while (i < calcul.Length && (calcul[i] != '*' && calcul[i] != '/' && calcul[i] != '+' && calcul[i] != '-'))
            {
                operande = operande + calcul[i]; // Copie des éléments à droite de l'opérateur.         
                i++;
                calcFlag[1] = i; // Mise à jour de la case de calcFlag comportant l'index où se trouve l'extrémité du membre droit du calcul.
            }
       
            return operande;
        }

        // Fonction chargée de découper le calcul passé en paramètre : retourne les membres de droite et de gauche.
        static string[] DecouperCalcul(string calcul, int operatorIndex, int[] calcFlag)
        {
            string[] calculElements = new string[2];

            calculElements[0] = GetOperandeGauche(calcul, operatorIndex, calcFlag);
            calculElements[1] = GetOperandeDroite(calcul, operatorIndex, calcFlag);

            return calculElements;
        }

        // Permet d'obtenir la position de l'opérateur prioritaire détecté.
        static int GetOperateurPrioIndex(string calcul)
        {
            int operateurPrioIndex = 0;

            for (int i = 0; i < calcul.Length; i++)
            {
                if (calcul[i] == '*' || calcul[i] == '/')
                {
                    operateurPrioIndex = i;
                }
            }

            return operateurPrioIndex;
        }

        // Vérifie la présence d'un opérateur prioritaire.
        static bool DetecterOperateurPrio(string calcul)
        {
            bool operateurPrioDetect = false;

            for (int i = 0; i < calcul.Length; i++)
            {
                if (calcul[i] == '*' || calcul[i] == '/')
                {
                    operateurPrioDetect = true;
                }           
            }

            return operateurPrioDetect;
        }

        // Retourne l'index où se trouve l'opérateur standard.
        static int GetOperateurIndex(string calcul)
        {
            int operateurPrioIndex = 0;

            for (int i = 0; i < calcul.Length; i++)
            {
                if (calcul[i] == '+' || calcul[i] == '-')
                {
                    operateurPrioIndex = i;
                }
            }

            return operateurPrioIndex;
        }

        // Détecte la présence d'un opérateur de alcul stantard.
        static bool DetecterOperateurs(string calcul)
        {
            bool operateurDetect = false;

            for (int i = 0; i < calcul.Length; i++)
            {
                if (calcul[i] == '+' || calcul[i] == '-')
                {
                    operateurDetect = true;
                }
            }

            return operateurDetect;
        }

        // Calcul d'une opération à opérateur prioritaire.
        static string CalculerOpePrio(string calcul)
        {
            string calcToDo = calcul;

            while (DetecterOperateurPrio(calcToDo))
            {
                int[] calcFlag = new int[2]; // Tableau contenant les coordonnée du calcul prioritaire à réaliser.
                int operatorIndex = GetOperateurPrioIndex(calcToDo); // Récupération de la position de l'opérateur.
                char operateur = calcToDo[operatorIndex]; // Récupération de l'opérateur
                string[] calcElements = DecouperCalcul(calcToDo, operatorIndex, calcFlag); // Obtention des membres gauche et droit du calcul
                double calcRes = RealiserOperation(calcElements, operateur); // Réalisation de l'opération.
                calcToDo = MaJOperation(calcToDo, calcFlag, calcRes.ToString()); // Mise à jour de la chaîne représentant le calcul.

                Console.WriteLine(calcToDo);
                Console.ReadLine();
            }

            return calcToDo;
        }

        // Calcul d'une opération à opérateur simple.
        static string CalculerOpeSimple(string calcul)
        {
            string calcToDo = calcul;

            while (DetecterOperateurs(calcToDo))
            {
                int[] calcFlag = new int[2]; // Tableau contenant les coordonnée du calcul.
                int operatorIndex = GetOperateurIndex(calcToDo);// Récupération de la position de l'opérateur.
                char operateur = calcToDo[operatorIndex];// Récupération de l'opérateur
                string[] calcElements = DecouperCalcul(calcToDo, operatorIndex, calcFlag);// Obtention des membres gauche et droit du calcul
                double calcRes = RealiserOperation(calcElements, operateur);// Réalisation de l'opération.
                calcToDo = MaJOperation(calcToDo, calcFlag, calcRes.ToString());// Mise à jour de la chaîne représentant le calcul.

                Console.WriteLine(calcToDo);
                Console.ReadLine();
            }

            return calcToDo;
        }

        // Réalise le calcul envoyé en paramètre. 
        static string EffectuerCalcul(string calcul)
        {
            string calcToDo = calcul;

            calcToDo = CalculerOpePrio(calcToDo);
            calcToDo = CalculerOpeSimple(calcToDo);

            return calcToDo;
        }

        // Détecte si des parenthèses sont toujours présentes dans l'opération.
        static bool DetecterParentheses(string operation)
        {
            bool parentheseDetect = false;

            for (int i = 0; i < operation.Length; i++)
            {
                if (operation[i] == '(')
                {
                    parentheseDetect = true;
                }
            }

            return parentheseDetect;
        }

        // Ajout de parenthèses au début et à la fin du calcul.
        static string AjoutParentheses(string operation)
        {
            string tmpOperation = "(";
            tmpOperation += operation;
            tmpOperation += ")";

            return tmpOperation;
        }

        // Retourne la position de la parenthèse fermante trouvée. 
        static int GetPosFermante(string operation)
        {
            int fermantePos = 0;
            bool firstFermante = false;

            for (int i = 0; i < operation.Length; i++)
            {
                if (operation[i] == ')' && firstFermante == false)
                {
                    fermantePos = i;
                    firstFermante = true;
                }
            }

            return fermantePos;
        }

        // Retourne la position de la dernière parenthèse ouvrante trouvée.
        static int GetPosOuvrante(string operation)
        {
            int ouvrantePos = 0;

            for(int i = 0; i < operation.Length; i++)
            {
                if (operation[i] == '(')
                {
                    ouvrantePos = i;
                }
            }

            return ouvrantePos;
        }

        // Permet de récupérer l'opération entre parenthèses.
        static string IsolerBlocParentheses(string operation, int ouvrantePos, int fermantePos)
        {
            string blocParenthese = ""; 

            for(int i = ouvrantePos + 1; i < fermantePos; i++)
            {
                blocParenthese += operation[i];
            }

            return blocParenthese;
        }

        // Retire les parenthèses et remplace l'opération et les "(" et ")" par le résultat du calcul.
        static string RetirerParentheses(string operation, int ouvrantePos, int fermantePos, string resCalc)
        {
            string majBloc = "";

            for (int i = 0; i < ouvrantePos; i++)
            {
                majBloc += operation[i];
            }

            majBloc += resCalc;

            for (int i = fermantePos + 1; operation.Length > i; i++)
            {
                majBloc += operation[i];
            }

            return majBloc;
        }

        // Fonction principale de la calculatrice : résolution d'opération avec parenthèses.
        static void ResoudreOperation(string operation)
        {
            Console.WriteLine(operation);
            operation = FiltrerVirgules(operation); // Filtrage des "." en les remplaçant pas ","
            operation = AjoutParentheses(operation); // Ajout de parenthèses

            while (DetecterParentheses(operation)) // Boucle tant que parenthèses
            {
                int ouvrantePos = GetPosOuvrante(operation); // Position de la  "("
                int fermantePos = GetPosFermante(operation); // Position de la première ")".

                string blocToCalc = IsolerBlocParentheses(operation, ouvrantePos, fermantePos); // Isole le bloc entre parenthèses         
                string operationBlocRes = EffectuerCalcul(blocToCalc); // Effectue le calcul du bloc.

                // Suppression des parenthèses et mise à jour du calcul
                operation = RetirerParentheses(operation, ouvrantePos, fermantePos, operationBlocRes); 
            }

            Console.WriteLine("Le résultat final est : " + operation + "\n");
        }

        // Saisi de l'opération par l'utilisateur.
        static string SaisirOperation()
        {
            Console.Write("Entrez un calcul (sans négatifs - tapez 'end' pour sortir de l'appli) : ");
            string operation = Console.ReadLine();
            return operation;
        }

        static void Main(string[] args)
        {
            string operation = "";
            
            while(operation != "end")
            {
                operation = SaisirOperation();
                ResoudreOperation(operation);
            }        
        }
    }
}